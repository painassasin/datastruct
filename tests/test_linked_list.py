import io
import sys
import unittest

from datastruct import LinkedList


class TestLinkedList(unittest.TestCase):
    def setUp(self) -> None:
        self.ll = LinkedList()
        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput

    def test_linked_list_head(self):
        self.ll.insert_beginning({'id': 1})
        self.ll.insert_at_end({'id': 2})
        self.ll.insert_at_end({'id': 3})
        self.ll.insert_beginning({'id': 0})

        self.assertEqual(self.ll.head.data, {'id': 0})
        self.assertEqual(self.ll.head.next_node.data, {'id': 1})
        self.assertEqual(self.ll.head.next_node.next_node.data, {'id': 2})
        self.assertEqual(self.ll.head.next_node.next_node.next_node.data, {'id': 3})

    def test_linked_list_tail(self):
        self.ll.insert_at_end({'id': 0})
        self.ll.insert_at_end({'id': 1})

        self.assertEqual(self.ll.tail.data, {'id': 1})
        self.assertIsNone(self.ll.tail.next_node)

    def test_to_list(self):
        self.ll.insert_beginning({'id': 1, 'username': 'lazzy508509'})
        self.ll.insert_at_end({'id': 2, 'username': 'mik.roz'})
        self.ll.insert_beginning({'id': 0, 'username': 'serebro'})

        self.assertListEqual(
            list1=self.ll.to_list(),
            list2=[
                {'id': 0, 'username': 'serebro'},
                {'id': 1, 'username': 'lazzy508509'},
                {'id': 2, 'username': 'mik.roz'},
            ]
        )

    def test_get_data_by_id(self):
        self.ll.insert_beginning({'id': 1, 'username': 'lazzy508509'})
        self.ll.insert_at_end({'id': 2, 'username': 'mik.roz'})
        self.ll.insert_beginning({'id': 0, 'username': 'serebro'})

        self.assertDictEqual(self.ll.get_data_by_id(1), {'id': 1, 'username': 'lazzy508509'})

    def test_get_data_by_id_not_found(self):
        sys.stdout = io.StringIO()
        self.ll.insert_beginning({'id': 1, 'username': 'lazzy508509'})
        self.ll.insert_at_end({'age': 20, 'username': 'mik.roz'})

        data = self.ll.get_data_by_id(20)

        self.assertIsNone(data)
        self.assertEqual(
            first=sys.stdout.getvalue(),
            second='Данные не являются словарем или в словаре нет id.\n'
        )
