import unittest

from datastruct import Stack


class TestStack(unittest.TestCase):
    def setUp(self) -> None:
        self.stack = Stack()

    def test_push(self):
        self.stack.push('data_1')
        self.stack.push('data_2')

        self.assertEqual(self.stack.top.data, 'data_2')
        self.assertEqual(self.stack.top.next_node.data, 'data_1')

    def test_pop_non_empty_stack(self):
        self.stack.push('data_1')
        self.stack.push('data_2')

        data_2 = self.stack.pop()
        data_1 = self.stack.pop()

        self.assertIsNone(self.stack.top)
        self.assertEqual(data_1, 'data_1')
        self.assertEqual(data_2, 'data_2')

    def test_pop_empty_stack(self):
        data = self.stack.pop()
        self.assertIsNone(data)
