import unittest

from datastruct import Queue


class TestQueue(unittest.TestCase):
    def setUp(self) -> None:
        self.queue = Queue()

    def test_enqueue_with_on_element(self):
        element = 'data'

        self.queue.enqueue(element)

        self.assertEqual(self.queue.head, self.queue.tail)
        self.assertEqual(self.queue.head.data, element)
        self.assertEqual(self.queue.tail.data, element)
        self.assertIsNone(self.queue.head.next_node)
        self.assertIsNone(self.queue.tail.next_node)

    def test_enqueue_head(self):
        self.queue.enqueue('data1')
        self.queue.enqueue('data2')
        self.queue.enqueue('data3')

        self.assertEqual(self.queue.head.data, 'data1')
        self.assertEqual(self.queue.head.next_node.data, 'data2')
        self.assertEqual(self.queue.head.next_node.next_node.data, 'data3')
        self.assertIsNone(self.queue.head.next_node.next_node.next_node)

    def test_enqueue_tail(self):
        self.queue.enqueue('data1')
        self.queue.enqueue('data2')
        self.queue.enqueue('data3')

        self.assertEqual(self.queue.tail.data, 'data3')
        self.assertIsNone(self.queue.tail.next_node)

    def test_dequeue(self):
        self.queue.enqueue('data1')
        self.queue.enqueue('data2')
        self.queue.enqueue('data3')

        self.assertEqual(self.queue.dequeue(), 'data1')
        self.assertEqual(self.queue.dequeue(), 'data2')
        self.assertEqual(self.queue.dequeue(), 'data3')

    def test_dequeue_empty_queue(self):
        self.assertIsNone(self.queue.dequeue())
