from dataclasses import dataclass
from typing import Any, TypeVar

TNode = TypeVar('TNode', bound='Node')


@dataclass
class Node:
    data: Any
    next_node: TNode | None = None


class Stack:
    def __init__(self, top: Node | None = None):
        self.__top = top

    @property
    def top(self) -> Node | None:
        return self.__top

    def push(self, data: Any) -> None:
        """Добавляет элемент в начало стека."""
        node = Node(data)
        if not self.is_empty():
            node.next_node = self.top
        self.__top = node

    def pop(self) -> Any | None:
        """
        Удаляет из стека верхний элемент (последний добавленный), реализуя правило LIFO.
        Если в стеке нет элементов, вернет None.
        """
        if not self.is_empty():
            data = self.top.data
            self.__top = self.top.next_node
            return data

    def is_empty(self) -> bool:
        return self.top is None
