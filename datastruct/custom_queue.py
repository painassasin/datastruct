from dataclasses import dataclass
from typing import Any, TypeVar

TNode = TypeVar('TNode', bound='Node')


@dataclass
class Node:
    data: Any
    next_node: TNode | None = None


class Queue:
    def __init__(self):
        self.head: Node | None = None
        self.tail: Node | None = None

    def enqueue(self, data: Any) -> None:
        """Добавление данных в очередь."""
        new_node = Node(data=data)

        if self.is_empty():
            self.head = self.tail = new_node
        else:
            self.tail.next_node = new_node
            self.tail = new_node

    def dequeue(self) -> Any | None:
        """
        Удаляет из очереди крайний левый элемент (первый добавленный),
        реализуя правило FIFO (First In, First Out)
        и возвращает данные удаленного экземпляра класса `Node`.
        """
        if self.head:
            data = self.head.data
            self.head = self.head.next_node
            return data

    def is_empty(self) -> bool:
        return self.tail is None
