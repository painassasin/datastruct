from dataclasses import dataclass
from typing import TypeVar, Any

TNode = TypeVar('TNode', bound='Node')


@dataclass
class Node:
    data: dict
    next_node: TNode | None = None


class LinkedList:
    def __init__(self):
        self.head: Node | None = None
        self.tail: Node | None = None

    def __iter__(self):
        self.__current = self.head
        return self

    def __next__(self) -> dict:
        current_node: Node | None = self.__current
        if not current_node:
            raise StopIteration
        self.__current = self.__current.next_node
        return current_node.data

    def insert_beginning(self, data: dict) -> None:
        new_node = Node(data, next_node=self.head)
        self.head = new_node
        if self.is_empty():
            self.tail = new_node

    def insert_at_end(self, data: dict) -> None:
        new_node = Node(data)
        if not self.is_empty():
            self.tail.next_node = new_node
        self.tail = new_node

    def is_empty(self) -> bool:
        return self.tail is None

    def to_list(self) -> list[dict]:
        """Возвращает список с данными, содержащимися в односвязном списке LinkedList."""
        return list(self)

    def get_data_by_id(self, item_id: Any) -> Any | None:
        """
        Возвращает первый найденный в LinkedList словарь с ключом id,
        значение которого равно переданному в метод значению.
        """
        for data in self:
            if self._get_data_id(data) == item_id:
                return data

    @staticmethod
    def _get_data_id(data: dict) -> Any | None:
        try:
            return data['id']
        except (KeyError, TypeError):
            print('Данные не являются словарем или в словаре нет id.')

    def print_ll(self):  # pragma: no cover
        ll_string = ''
        node = self.head
        if node is None:
            print(None)
        while node:
            ll_string += f' {str(node.data)} ->'
            node = node.next_node

        ll_string += ' None'
        print(ll_string)
